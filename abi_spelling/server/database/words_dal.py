import sqlite3
from pathlib import Path
import re


class WordsDatabase:
    DATABASE_PATH = f'{Path(__file__).parent}/words.db'

    def __init__(self):
        self.database = sqlite3.connect(self.DATABASE_PATH)

    def mk_table(self, table_name, columns: dict):
        table_columns = ""
        for key in columns:
            table_columns += f'{key} {columns[key]},\n'
        table_columns = re.sub(r',\n$', '', table_columns)

        self.database.execute(
            f'''
                CREATE TABLE {table_name}
                ({table_columns});
            '''
        )

    def insert(self, table, content: dict):
        query = f'INSERT INTO {table} ('
        input_values = 'VALUES('
        for key in content:
            query += f'{key},'
            input_values += f'''{f'"{content[key]}"' if type(content[key]) == str else content[key]},'''
        query = re.sub(r',$', r')\n', query)
        input_values = re.sub(r',$', r')', input_values)
        query += input_values
        cur = self.database.cursor()
        cur.execute(query)
        self.database.commit()

    def delete_task(self, table, word):
        sql = f'''DELETE FROM {table} WHERE word="{word}"'''
        cur = self.database.cursor()
        cur.execute(sql)
        self.database.commit()

    def get_table_contents(self, table):
        cur = self.database.cursor()
        cur.execute(f'''Select * from {table}''')
        rows = cur.fetchall()
        return rows

    def format_words_for_return(self):
        rows = self.get_table_contents('WORDS')
        words = ''
        for row in rows:
            words += f'{row[0]},'
        words = re.sub(r',$', '', words)
        return words


if __name__ == '__main__':
    db = WordsDatabase()
    db.mk_table('WORDS', {'word': 'TEXT NOT NULL PRIMARY KEY'})
    # cursor = db.database.execute('select * from WORDS')
    # names = list(map(lambda x: x[0], cursor.description))
    # print(names)
    # print(db.format_words_for_return())
    # db.insert(
    #     'WORDS',
    #     {
    #         'word': 'important'
    #     }
    # )
    # rows = db.get_table_contents('WORDS')
    # for r in rows:
    #     print(r)
    # db.delete_task('WORDS', 'important')
