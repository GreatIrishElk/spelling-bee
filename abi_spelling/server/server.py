import http.server
import re
from pathlib import Path
import socket
from abi_spelling.server.database.words_dal import WordsDatabase

# HOSTNAME = socket.gethostname()
# print(HOSTNAME)
# IP_ADDRESS = socket.gethostbyname(HOSTNAME)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.connect(('8.8.8.8', 80))
IP_ADDRESS = sock.getsockname()[0]
PORT = 8080


class AbiServer(http.server.SimpleHTTPRequestHandler):
    PATH_TO_WEBSITE = f"{Path(__file__).parent.parent}/website"

    class WebPages:
        NEXT_WORD = f'website/next_word.html'
        INSERT_WORD = f'website/add_words.html'
        LIST_WORDS = f'website/list_words.html'

    class StyleSheets:
        STYLESHEET = f'website/style.css'

    class Scripts:
        SCRIPTS = f'website/js'
        MAIN = f'{SCRIPTS}/main.js'
        BLANK_LETTERS = f'{SCRIPTS}/blank_letters.js'
        RANDOM_NUMBER = f'{SCRIPTS}/random_number.js'
        RANDOM_ELEMENT = f'{SCRIPTS}/random_array_element.js'
        GET_WORD = f'{SCRIPTS}/get_word.js'
        COMPARE_WORD = f'{SCRIPTS}/compare_words.js'
        ATTEMPT = f'{SCRIPTS}/attempt.js'
        COOKIES = f'{SCRIPTS}/cookies.js'
        WORDS = f'{SCRIPTS}/words.js'
        REQUESTS = f'{SCRIPTS}/requests.js'
        DATABASE = f'{SCRIPTS}/database.js'

    def do_GET(self):
        path = self.path
        if re.search('^/next-word$', self.path):
            self.path = self.WebPages.NEXT_WORD
        elif re.search('^/insert-word$', self.path):
            self.path = self.WebPages.INSERT_WORD
        elif re.search('^/next-word/style.css$', self.path):
            self.path = self.StyleSheets.STYLESHEET
        elif re.search('^/list-words', self.path):
            self.path = self.WebPages.LIST_WORDS
        elif re.search('(^/set-word$)|(main.js$)', self.path):
            self.path = self.Scripts.MAIN
        elif re.search('blank_letters.js$', self.path):
            self.path = self.Scripts.BLANK_LETTERS
        elif re.search('random_number.js$', self.path):
            self.path = self.Scripts.RANDOM_NUMBER
        elif re.search('random_array_element.js$', self.path):
            self.path = self.Scripts.RANDOM_ELEMENT
        elif re.search('get_word.js$', self.path):
            self.path = self.Scripts.GET_WORD
        elif re.search('(^/compare-words$)|(compare_word.js$)', self.path):
            self.path = self.Scripts.COMPARE_WORD
        elif re.search('attempt.js$', self.path):
            self.path = self.Scripts.ATTEMPT
        elif re.search('cookies.js$', self.path):
            self.path = self.Scripts.COOKIES
        elif re.search('words.js$', self.path):
            self.path = self.Scripts.WORDS
        elif re.search('requests.js', self.path):
            self.path = self.Scripts.REQUESTS
        elif re.search('(^/database$)|(database.js$)', self.path):
            self.path = self.Scripts.DATABASE
        elif re.search('^/add-word=', self.path):
            db = WordsDatabase()
            db.insert('WORDS', {'word': self.path.split('=')[-1]})
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes('', 'utf-8'))
            # TODO: Return status message of weather adding to database was successful.
        elif re.search('^/get-words', self.path):
            db = WordsDatabase()
            rows = db.format_words_for_return()
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes(rows, 'utf-8'))
        elif re.search('^/remove=', self.path):
            words = self.path.split('=')[-1]
            db = WordsDatabase()
            for word in words.split(','):
                db.delete_task('WORDS', word)
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes('', 'utf-8'))
        if path != self.path:
            return http.server.SimpleHTTPRequestHandler.do_GET(self)


if __name__ == '__main__':
    print(f'Starting server on {IP_ADDRESS}:{PORT}')
    server = http.server.HTTPServer((IP_ADDRESS, PORT), AbiServer)
    server.serve_forever()
