// want to create functions for downloading words from a server

// const baseURL = 'http://192.168.0.19:8080'
const baseURL = 'http://78.18.54.59:8080'

export function request(url) {
    var xmlHTTP = new XMLHttpRequest();
    xmlHTTP.open( "GET", url, false );
    xmlHTTP.send();
    return xmlHTTP.responseText;
}

export function getWords() {
    const response = request(`${baseURL}/get-words`);
    return response.split(',');
}

export function removeWords(words) {
    let deleteWords = '';
    console.log(words);
    for (const word of words) {
        console.log(word);
        deleteWords += `${word},`;
    }
    deleteWords = deleteWords.replace(/,$/, '');
    console.log(deleteWords);
    request(`${baseURL}/remove=${deleteWords}`);
}

export function addWord(word) {
    request(`${baseURL}/add-word=${word}`);
}