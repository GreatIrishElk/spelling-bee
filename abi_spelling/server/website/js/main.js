import { getWords } from './requests.js'
import { attempt } from './attempt.js'
import { addWordsToCookie, hasCookie, selectRandomWord } from './cookies.js'
import { setNextWord } from './words.js'

main();

function main() {
  console.log('Executing main function.');
  addWordsToCookie(getWords());
  if (!hasCookie('current-word')) {
    selectRandomWord();
  }
  setNextWord();
  attempt();
}
