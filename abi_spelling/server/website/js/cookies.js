import { randomElement } from "./random_array_element.js"

export function getCookies() {
  return document.cookie;
}

export function getCookie(name) {
  console.log(`Getting cookie: ${name}.`);
  let cookies = getCookies();
  cookies = cookies.split(';');
  for (let cookie of cookies) {
    cookie = cookie.trim();
    if (cookie.includes(name)) {
      return cookie;
    }
  }
}

export function selectRandomWord() {
  let cookie = getCookie('words');
  let words = cookie.split('=')[1].split(':');
  let word = randomElement(words);
  console.log(`Selected word: ${word}`);
  writeCookie('current-word', word, '78.18.54.59', '');
}

export function getCurrentWord() {
  let cookie = getCookie('current-word');
  return cookie.split(',')[0].split('=')[1];
}

export function getNumberOfLettersToBlank() {
  const cookie = getCookie('current-word');
  return parseInt(cookie.split(',')[1]);
}

export function hasCookie(name) {
  return document.cookie.split(';').some(c => {
        return c.trim().startsWith(name + '=');
    });
}

export function writeCookie(name, content, domain, path) {
  console.log(`Writing cooke: ${name}`);
  document.cookie = `${name}=${content};${domain};${path};${expires()}`
}

function expires() {
  var now = new Date();
  var time = now.getTime();
  var expireTime = time + 1000*360000;
  now.setTime(expireTime);
  return `expires=${now.toUTCString()}`;
}

function isWordInWordsCookie(word) {
  const wordsCookie = getWords();
  for (const wordFromCookie of wordsCookie) {
    if (word == wordFromCookie[0]) {
      return true;
    }
  }
  return false;
}

export function addWordsToCookie(words) {
  // Cookie Headings
  // Word,numberToBlank
  console.log('Adding words to cookie words.');
  if (hasCookie('words')) {
    var cookie = getCookie('words').split('=')[1];
  } else {
    var cookie = '';
  }
  for (const word of words) {
    if (!cookie.match(word)) {
      console.log(`Adding ${word} to cookie.`);
      cookie += `${word},1:`;
    }
  }
  cookie = cookie.replace(/:$/, '');
  writeCookie('words', cookie, '', '')
}

function addWordToCookie(word) {
  if (hasCookie('words')) {
    let cookie = getCookie('words');
    console.log(cookie);
  }
}

export function removeWordsFromCookies(words) {
  if (hasCookie('words')) {
    var cookie = getCookie('words').split('=')[1];
    // var cookieArray = cookie.split(':')
    for (const word of words) {
      if (cookie.match(word)) {
        removeWordFromCookie(word);
      }
    }
  }
}

function removeWordFromCookie(word) {
  var cookie = getCookie('words').split('=')[1];
  cookie = cookie.split(':');
  var index = null;
  for (var foo of cookie) {
    if (foo.split(',')[0] == word) {
      index = cookie.indexOf(foo)
    }
  }
  cookie.splice(index, 1);
  var updatedCookie = '';
  for (const foo of cookie) {
    updatedCookie += `${foo}:`;
  }
  updatedCookie = updatedCookie.replace(/:$/, '');
  writeCookie('words', updatedCookie, '', '');
}

export function incrementNumberToBlank() {
  const words = getWords();
  const current_word = getCurrentWord();
  const numberOfLettersToBlank = getNumberOfLettersToBlank();
  let newContent = '';
  for (let word of words) {
    let row = '';
    if (shouldIncrement(current_word, word, numberOfLettersToBlank)) {
      row =`${word.split(',')[0]},${numberOfLettersToBlank + 1}`;
    } else {
      row = word;
    }
    if (!islastElementInArray(words, word)) {
      row += ':';
    }
    newContent += row;
  }
  writeCookie('words', newContent, '78.18.54.59', '');
}

function shouldIncrement(current_word, word, numberOfLettersToBlank) {
  return (word.split(',')[0] == current_word && (numberOfLettersToBlank + 1) <= current_word.length);
}

function getWords() {
  return getCookie('words').split('=')[1].split(':');
}

function islastElementInArray(array, element) {
  return (array.length == (array.indexOf(element) + 1));
}

export function deleteCookie(name) {
  if (hasCookie(name)) {
    document.cookie = name + "=" + ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
  }
}
