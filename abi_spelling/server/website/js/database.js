import { addWord, getWords, removeWords } from './requests.js'
import { removeWordsFromCookies } from './cookies.js';


const listWordsURL = /list-words$/;
const addWordsURL = /insert-word$/;

if (document.URL.match(listWordsURL)) {
    listWords();
    document.getElementById('delete').addEventListener('click', deleteWords);
} else if (document.URL.match(addWordsURL)){
    document.getElementById('submit').addEventListener('click', submitWord);
}   

function submitWord() {
    const word = document.getElementById('add-word').value;
    addWord(word);
    displayAddedWord(word);
}

function displayAddedWord(word) {
    let placeForWords = document.getElementById('added-words');
    let wordDiv = document.createElement('p');
    wordDiv.innerHTML = `Added: ${word}`;
    placeForWords.append(wordDiv);
}

function listWords() {
    const words = getWords();
    const paragraph = document.createElement('form');
    document.getElementById('words').append(paragraph);
    paragraph.id = 'list-words';
    for (let word of words) {
        appendWord(word);
    }
    const submit = document.createElement('input');
    submit.type = 'button';
    submit.id = 'delete';
    submit.value = 'Delete';
    paragraph.append(submit);
}

function appendWord(word) {
    const paragraph = document.getElementById('list-words');
    const radioButton = document.createElement('input');
    radioButton.type = 'checkbox';
    radioButton.name = 'words';
    radioButton.value = word;
    const wordElement = document.createElement('label');
    wordElement.innerHTML = `${word}`;

    paragraph.append(radioButton);
    paragraph.append(wordElement);
    paragraph.append(document.createElement('br'));
}

function deleteWords() {
    const checkBoxes = document.getElementsByName('words');
    let wordsToDelete = [];
    for (const checkBox of checkBoxes) {
        if (checkBox.checked) {
            wordsToDelete.push(checkBox.value);
        }
    }
    removeWords(wordsToDelete);
    removeWordsFromCookies(wordsToDelete)
    removeWordsFromTag();
    listWords();
}

function removeWordsFromTag() {
    let topDiv = document.getElementById('words');
    let child = document.getElementById('list-words')
    topDiv.removeChild(child);
}