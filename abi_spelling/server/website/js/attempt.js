import { updateWord } from './words.js'

export function attempt() {
  console.log('Time to make an attempt.');
  delayFunction(updateWord)
}

export function delayFunction(func) {
  const timeout = setTimeout(func, 10000);
}
