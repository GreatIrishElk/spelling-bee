import { getCurrentWord, getNumberOfLettersToBlank, selectRandomWord } from './cookies.js'
import { randomNumber } from './random_number.js'

export function setNextWord() {
  const word = getCurrentWord();
  console.log(`Setting word as: ${word}.`);
  document.getElementById("next_word").innerHTML = word;
}

export function indexesToBlanking() {
  let elementsToReplace = [];
  const word = getCurrentWord();
  const letters = word.split('');
  const numberLetters = getNumberOfLettersToBlank();
  console.log(`Blanking ${numberLetters}`);
  for (let index=0; index < numberLetters; index++) {
    let number = randomNumber(letters.length);
    while (elementsToReplace.includes(number)) {
      number = randomNumber(letters.length);
    }
    elementsToReplace.push(number);
  }
  elementsToReplace.sort();
  return elementsToReplace;
}

export function updateWord() {
  const elementsToReplace = indexesToBlanking();
  const word = getCurrentWord();
  console.log(`Blanking letters: ${elementsToReplace} from: ${word}`);
  let letters = word.split('');
  let string = "";
  for (const index of elementsToReplace) {
    letters[index] = '*';
  }
  for (let element of letters) {
    string += element;
  }
  document.getElementById("next_word").innerHTML = string;
  document.getElementById('guess').disabled = false;
}

export function clear() {
  console.log('Clearing elements.');
  document.getElementById('original').innerHTML = '';
  document.getElementById('originalText').style.display = 'none';
  document.getElementById('attempt').style.display = 'none';
  document.getElementById('congrats').style.display = 'none';
  document.getElementById('next').style.display = 'none';
  document.getElementById('guess').value = '';
  document.getElementById('guess').disabled = true;
}

export function getWords() {
  let words = document.getElementById('words').innerHTML.split(',');
  for ( let element of words) {
    words[words.indexOf(element)] = element.trim();
  }
  return words;
}
