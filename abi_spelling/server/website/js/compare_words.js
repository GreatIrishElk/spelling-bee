import { attempt } from './attempt.js'
import { getCurrentWord, selectRandomWord, incrementNumberToBlank } from './cookies.js'
import { setNextWord, clear } from './words.js'

document.getElementById('submit').addEventListener('click', compareWords);
document.getElementById('next').addEventListener('click', nextWord);

export function compareWords() {
  const original_word = getCurrentWord()
  if (original_word == getGuess().toLowerCase()) {
    showCongrats();
    incrementNumberToBlank();
  } else {
    console.log('Showing original and attempt.');
    showOriginal();
    showAttempt();
  }
  document.getElementById('next').style.display = 'block';
}

function nextWord() {
  selectRandomWord();
  clear();
  setNextWord();
  attempt();
}

function showCongrats() {
  document.getElementById('congrats').style.display = 'block'
}

function originalWord() {
  return getCurrentWord();
}

function getGuess() {
  return document.getElementById('guess').value;
}

function showOriginal() {
  const current_word = getCurrentWord();
  console.log(`Showing ${current_word}`);
  document.getElementById('original').innerHTML = current_word;
  document.getElementById('originalText').style.display = 'block';
}

function showAttempt() {
  document.getElementById('attempt').innerHTML = `Attempt: ${getGuess()}`;
  document.getElementById('attempt').style.display = 'block';
}
